# gitlab-conan-proxy

Basic HTTP proxy to fix GitLab not handling `package/version` format for Conan packages.

**Warning: somewhat clunky code, not meant for production, use it at your own risks!**

## Rationale

This is meant to intercept calls to the `/api/v4/packages/conan/` endpoints, and sanitize GitLab's responses before
forwarding them to the conan client (in that case, ignore the `package_username is invalid, package_channel is invalid`
error coming from GitLab which can't handle `package/version` dependency format).


## Instructions

Put the conan-proxy.py in some sane location (assuming `/usr/local/bin` here).

Then add the systemd unit `conan_proxy.service` in /etc/systemd/system/ (**don't forget to update the world-facing domain name in here**), issue a `systemctl daemon-reload` command to
make it available, and run `systemctl enable --now conan_proxy`.

Sample conf to put in the apache virtualhost in front of GitLab (similar stuff for nginx):

```
RewriteEngine on
RewriteRule ^(/api/v4/packages/conan/.*) http://localhost:8765/$1 [P]
```

Here 8765 is the port the proxy is meant to listen on (configured in the unit file).
