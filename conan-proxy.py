#!/usr/bin/env python3

import gzip
from http.server import SimpleHTTPRequestHandler
import requests
import sys
import socketserver


class Unbuffered(object):
    def __init__(self, stream):
        self.stream = stream

    def write(self, data):
        self.stream.write(data)
        self.stream.flush()

    def writelines(self, datas):
        self.stream.writelines(datas)
        self.stream.flush()

    def __getattr__(self, attr):
        return getattr(self.stream, attr)


sys.stdout = Unbuffered(sys.stdout)

gitlab_domain = sys.argv[1]
url_base = sys.argv[2]
listen_port = sys.argv[3]


class ConanProxy(SimpleHTTPRequestHandler):
    def _send_request(self, method):
        url = f"{url_base}{self.path}"

        length = int(self.headers.get("Content-Length", -1))
        data = None

        if length > 0:
            data = self.rfile.read(length)
        req_headers = self.headers
        req_headers["Host"] = gitlab_domain

        return method(url, data=data, headers=req_headers)

    def _send_response_headers(self, response):
        status_code = response.status_code
        if (
            response.status_code == 400
            and response.content
            == b'{"error":"package_username is invalid, package_channel is invalid"}'
        ):
            status_code = 404
        self.send_response(status_code)
        content = response.content
        if response.headers.get("Content-Encoding", "") == "gzip":
            content = gzip.compress(content)

        for header, value in response.headers.items():
            if header != "Transfer-Encoding" or value != "chunked":
                self.send_header(header, value)
            else:
                self.send_header("Content-Length", len(content))

        self.end_headers()

        return content

    def do_GET(self):
        try:
            response = self._send_request(requests.get)
            content = self._send_response_headers(response)
            self.wfile.write(content)
        except Exception as e:
            print(f"err_GET: {e}")

    def do_HEAD(self):
        try:
            response = self._send_request(requests.head)
            self._send_response_headers(response)
        except Exception as e:
            print(f"err_HEAD: {e}")

    def do_PUT(self):
        try:
            response = self._send_request(requests.put)
            content = self._send_response_headers(response)
            self.wfile.write(content)
        except Exception as e:
            print(f"err_PUT: {e}")

    def do_DELETE(self):
        try:
            response = self._send_request(requests.delete)
            self._send_response_headers(response)
        except Exception as e:
            print(f"err_DELETE: {e}")

    def do_POST(self):
        print("do_POST")

        try:
            response = self._send_request(requests.post)
            content = self._send_response_headers(response)
            self.wfile.write(content)
        except Exception as e:
            print(f"err_POST: {e}")


socketserver.ForkingTCPServer.allow_reuse_address = True
httpd = socketserver.ForkingTCPServer(("", listen_port), ConanProxy)
httpd.serve_forever()
